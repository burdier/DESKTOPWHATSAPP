import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton,QSystemTrayIcon,QStyle
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
from PyQt5 import QtWidgets,QtCore,QtWebEngineWidgets
import time
from threading import Thread


class App(object):
    def initUI(self,MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1400, 800)
        
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.webView = QtWebEngineWidgets.QWebEngineView(self.centralwidget)
        self.webView.setUrl(QtCore.QUrl("https://web.whatsapp.com/"))
        self.webView.setObjectName("webView")
        self.gridLayout.addWidget(self.webView, 0,0, 0, 0)
        MainWindow.setCentralWidget(self.centralwidget)
        # self.statusbar = QtWidgets.QStatusBar(MainWindow)
        #self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        ##self.on_click(MainWindow)
        # Init QSystemTrayIcon
        
    
  
    # def on_click(self,MainWindow):
    #     MainWindow.trayIcon = QtWidgets.QSystemTrayIcon(MainWindow)
    #     MainWindow.trayIcon.setIcon(MainWindow.style().standardIcon(QStyle.SP_ComputerIcon))
    #     MainWindow.trayIcon.show()
    #     MainWindow.trayIcon.showMessage(
    #         "Tray Program",
    #         "Application was minimized to Tray",
    #         QSystemTrayIcon.Information,
    #         2000
    #         )

    

    
 
if __name__ == '__main__':
    app = QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    MainWindow.setWindowFlags(QtCore.Qt.FramelessWindowHint)
    ex = App()
    ex.initUI(MainWindow)
    MainWindow.show()
    
    # t = Thread(target=ex.on_click, args=(MainWindow,))
    # t.start()

    sys.exit(app.exec_())
